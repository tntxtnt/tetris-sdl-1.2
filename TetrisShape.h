#ifndef TetrisShape_H
#define TetrisShape_H

#include <vector>
#include <string>

struct Point {
    int x;
    int y;
    Point(int x=0, int y=0) : x(x), y(y) {}
};

class TetrisShape
{
public:
    static const std::string Shapes;
    TetrisShape();
    TetrisShape(char c);
    ~TetrisShape();
    void rotate(bool isClockWised);
    const std::vector<Point>& getData()const {return data;}
    int getColor()const {return color;}
    void setColor(unsigned c) {color=c;}
    void setPosition(int x, int y) {px=x; py=y;}
    int x()const {return px;}
    int y()const {return py;}
    int size()const {return data.size();}
    const Point& operator[](int i)const {return data[i];}
    void randomize();
private:
    void init();
private:
    int px;
    int py;
    unsigned color;
    char shape;
    std::vector<Point> data;
};

#endif // TetrisShape_H
