#ifndef TETRISGRID_H
#define TETRISGRID_H

#include <vector>
#include "TetrisShape.h"

class TetrisGame
{
public:
    enum Status { END, PLAYING, ANIMATING, PAUSE };
    enum Dimension { ROWS=20, COLS=10 };
    enum MoveDirection { LEFT=0, RIGHT=1 };
    enum {MAX_LEVEL = 15};

    TetrisGame();
    ~TetrisGame();

    int at(int x, int y)const {return grid[x][y];}
    bool move(int direction, int steps=1);
    void rotateShape(bool isClockwised);
    bool dropDown();
    bool addShapeToGrid();
    bool checkFullLines();
    bool inGoodPosition();
    void updateScoreLevel();
    void collapsing();
    void animation();
    void reset();

    int getScore()const {return score;}
    int getLevel()const {return level;}
    const TetrisShape& getShape()const {return shape;}
    const TetrisShape& getNextShape()const {return nextShape;}
    int getStatus()const {return status;}

    void pause() { if (status==PLAYING) status = PAUSE; }
    void unPause() { if (status==PAUSE) status = PLAYING; }
    void increaseSpeed() {++baseLevel; baseLevel%=MAX_LEVEL;}
    void decreaseSpeed() {baseLevel+=MAX_LEVEL-1; baseLevel%=MAX_LEVEL;}
    void decreaseAnimatedFrame() { --animatedFrame; }
private:
    std::vector<int*> grid;
    TetrisShape shape;
    TetrisShape nextShape;
    std::vector<int> fullRows;
    int score;
    int level;
    int baseLevel;
    int animatedFrame;
    int status;
};

#endif // TETRISGRID_H
